package com.sidmas.module.datagenerator;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DataGeneratorApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(DataGeneratorApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(DataGeneratorApplication.class, args);

	}
}
