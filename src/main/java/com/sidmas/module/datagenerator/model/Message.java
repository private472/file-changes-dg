package com.sidmas.module.datagenerator.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
public class Message {
    @Getter @Setter
    private String userName;
    @Getter @Setter
    private String actionType;
    @Getter @Setter
    private String fileName;
    @Getter @Setter
    private String lastModified;
    @Getter @Setter
    private String filePreview;
}
