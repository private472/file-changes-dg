package com.sidmas.module.datagenerator.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class FileChangesMessageSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileChangesMessageSender.class);

    @Value("${request.url}")
    private String requestUrl;

    @Autowired
    private FileChangesMessageProvider fileChangesMessageProvider;

    @Scheduled(fixedRate = 5000)
    public String trySendMessage() {
        String providedFileChangesMessage = fileChangesMessageProvider.provideFileChangesMessage();
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> httpEntity = new HttpEntity<>(providedFileChangesMessage);
        return restTemplate.postForObject(requestUrl, httpEntity, String.class);
    }
}
