package com.sidmas.module.datagenerator.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sidmas.module.datagenerator.model.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.*;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@PropertySource("classpath:entry.properties")
@Service
public class FileChangesMessageProvider {

    @Value("${value.dirpath}")
    private String directoryPath;
    @Value("${value.previewfile}")
    private String filePreview;

    private static final Logger LOGGER = LoggerFactory.getLogger(FileChangesMessageProvider.class);

    private final Message message;

    @Autowired
    public FileChangesMessageProvider(Message message) {
        this.message = message;
    }

    public String provideFileChangesMessage() {

        LOGGER.debug("::Invoked provideFileChangesMessage()");

        ObjectMapper messageMapper = new ObjectMapper();
        String messageAsString = "";
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            WatchService watchService = FileSystems.getDefault().newWatchService();
            Path dirPath = Paths.get(directoryPath);
            dirPath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_MODIFY);
            WatchKey watchKey = watchService.take();
            Map<String, String> fileProperties = new HashMap<>();
            WatchEvent<?> event = watchKey.pollEvents().get(0);

                fileProperties.put("eventKind", event.kind().toString());
                fileProperties.put("fileName", event.context().toString());
                fileProperties.put("userName", System.getProperty("user.name"));
                fileProperties.put("lastModified", new Date().toString());

                LOGGER.debug("::Getting file and converting to Base64");

                Thread.sleep(3000);

                String imgExt = fileProperties.get("fileName")
                        .substring(fileProperties.get("fileName").lastIndexOf(".") + 1);
                String fileNameWithNoExt = fileProperties.get("fileName")
                        .substring(0, fileProperties.get("fileName").lastIndexOf("."));
                String actionName = "";

                LOGGER.debug("::FILE PATH = "+dirPath.toFile() + "\\" + fileProperties.get("fileName"));

                File file = new File(dirPath.toFile() + "\\" + fileProperties.get("fileName"));
                FileInputStream fileInputStream = new FileInputStream(file);
                ImageInputStream imageInputStream = ImageIO.createImageInputStream(fileInputStream);
                imageInputStream.flush();
                BufferedImage bufferedImage = ImageIO.read(imageInputStream);
                bufferedImage.flush();
                ImageIO.write(bufferedImage, imgExt, byteArrayOutputStream);

                byteArrayOutputStream.flush();

                byte[] imageAsBytesStream = byteArrayOutputStream.toByteArray();
                String imageAsBase64String = Base64.getEncoder().encodeToString(imageAsBytesStream);

                fileInputStream.close();

                LOGGER.debug("::Getting file changes");
                LOGGER.debug("::File properties:");

                fileProperties.forEach((key, value) -> LOGGER.debug(key + ":" + value));

                LOGGER.debug("base65:"+imageAsBase64String);

                if(fileProperties.get("eventKind").equals("ENTRY_MODIFY")){
                    actionName = "EDYCJA PROJEKTU";
                } else {
                    actionName = "UTWORZENIE PROJEKTU";
                }
                message.setActionType(actionName);
                message.setFileName(fileNameWithNoExt);
                message.setUserName(fileProperties.get("userName"));
                message.setLastModified(fileProperties.get("lastModified"));
                message.setFilePreview(imageAsBase64String);

                messageAsString = messageMapper.writeValueAsString(message);

                watchKey.reset();

        } catch (IOException | InterruptedException e) {
            LOGGER.error("::An error occured while handling file changes", e);
        }
        return messageAsString;
    }
}

